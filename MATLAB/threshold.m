function [result] = threshold(pPath) 
  lOriginal = imread(pPath);
  lGray = rgb2gray(lOriginal);
  result = imbinarize(lGray);
end