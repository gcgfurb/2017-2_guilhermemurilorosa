clear;
clc;

row = 4;
col = 4;
pos = 0;

pos= pos+1;
iOriginal = imread('ovo.jpg');
subplot(row,col,pos);imshow(iOriginal);title('original');

pos= pos+1;
iGray = rgb2gray(iOriginal);
subplot(row,col,pos);imshow(iGray);title('gray');

pos= pos+1;
iImbinarize = imbinarize(iGray);
subplot(row,col,pos);imshow(iImbinarize);title('imbinarize');

pos= pos+1;
iAdaptive = imbinarize(iGray, 'adaptive');
subplot(row,col,pos);imshow(iAdaptive);title('adaptive');

pos= pos+1;
iGlobal = imbinarize(iGray, 'global');
subplot(row,col,pos);imshow(iGlobal);title('global');

pos= pos+1;
iRoberts = edge(iGray,'roberts');
subplot(row,col,pos);imshow(iRoberts);title('roberts');

pos= pos+1;
iCanny = edge(iGray,'canny');
subplot(row,col,pos);imshow(iCanny);title('canny');

pos= pos+1;
iSobel = edge(iGray,'sobel');
subplot(row,col,pos);imshow(iSobel);title('sobel');

pos= pos+1;
iPrewitt = edge(iGray,'prewitt');
subplot(row,col,pos);imshow(iPrewitt);title('prewitt');

pos= pos+1;
iLog = edge(iGray,'log');
subplot(row,col,pos);imshow(iLog);title('log');

pos= pos+1;
iLog = edge(iGray,'approxcanny');
subplot(row,col,pos);imshow(iLog);title('approxcanny');

pos= pos+1;
iSobelBoth = edge(iGray, 'sobel' , 0.01, 'both');
subplot(row,col,pos);imshow(iSobelBoth);title('sobel both');