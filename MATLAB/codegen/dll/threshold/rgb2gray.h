/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * rgb2gray.h
 *
 * Code generation for function 'rgb2gray'
 *
 */

#ifndef RGB2GRAY_H
#define RGB2GRAY_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "threshold_types.h"

/* Function Declarations */
#ifdef __cplusplus

extern "C" {

#endif

  extern void rgb2gray(const emxArray_uint8_T *X, emxArray_uint8_T *I);

#ifdef __cplusplus

}
#endif
#endif

/* End of code generation (rgb2gray.h) */
