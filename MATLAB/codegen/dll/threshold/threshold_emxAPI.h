/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * threshold_emxAPI.h
 *
 * Code generation for function 'threshold_emxAPI'
 *
 */

#ifndef THRESHOLD_EMXAPI_H
#define THRESHOLD_EMXAPI_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "threshold_types.h"

/* Function Declarations */
#ifdef __cplusplus

extern "C" {

#endif

  extern emxArray_boolean_T *emxCreateND_boolean_T(int numDimensions, int *size);
  extern emxArray_boolean_T *emxCreateWrapperND_boolean_T(boolean_T *data, int
    numDimensions, int *size);
  extern emxArray_boolean_T *emxCreateWrapper_boolean_T(boolean_T *data, int
    rows, int cols);
  extern emxArray_boolean_T *emxCreate_boolean_T(int rows, int cols);
  extern void emxDestroyArray_boolean_T(emxArray_boolean_T *emxArray);
  extern void emxInitArray_boolean_T(emxArray_boolean_T **pEmxArray, int
    numDimensions);

#ifdef __cplusplus

}
#endif
#endif

/* End of code generation (threshold_emxAPI.h) */
