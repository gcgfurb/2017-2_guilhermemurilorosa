/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * imbinarize.h
 *
 * Code generation for function 'imbinarize'
 *
 */

#ifndef IMBINARIZE_H
#define IMBINARIZE_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "threshold_types.h"

/* Function Declarations */
#ifdef __cplusplus

extern "C" {

#endif

  extern void imbinarize(const emxArray_uint8_T *I, emxArray_boolean_T *BW);

#ifdef __cplusplus

}
#endif
#endif

/* End of code generation (imbinarize.h) */
