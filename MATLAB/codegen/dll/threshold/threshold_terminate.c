/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * threshold_terminate.c
 *
 * Code generation for function 'threshold_terminate'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "threshold.h"
#include "threshold_terminate.h"

/* Function Definitions */
void threshold_terminate(void)
{
  /* (no terminate code required) */
}

/* End of code generation (threshold_terminate.c) */
