/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * threshold_initialize.h
 *
 * Code generation for function 'threshold_initialize'
 *
 */

#ifndef THRESHOLD_INITIALIZE_H
#define THRESHOLD_INITIALIZE_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "threshold_types.h"

/* Function Declarations */
#ifdef __cplusplus

extern "C" {

#endif

  extern void threshold_initialize(void);

#ifdef __cplusplus

}
#endif
#endif

/* End of code generation (threshold_initialize.h) */
