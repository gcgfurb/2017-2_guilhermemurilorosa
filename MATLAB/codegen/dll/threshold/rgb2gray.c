/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * rgb2gray.c
 *
 * Code generation for function 'rgb2gray'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "threshold.h"
#include "rgb2gray.h"
#include "threshold_emxutil.h"
#include "libmwrgb2gray_tbb.h"

/* Function Definitions */
void rgb2gray(const emxArray_uint8_T *X, emxArray_uint8_T *I)
{
  int i1;
  emxArray_uint8_T *inputImage;
  short origSize[3];
  int loop_ub;
  for (i1 = 0; i1 < 3; i1++) {
    origSize[i1] = (short)X->size[i1];
  }

  emxInit_uint8_T(&inputImage, 3);
  i1 = I->size[0] * I->size[1];
  I->size[0] = origSize[0];
  I->size[1] = origSize[1];
  emxEnsureCapacity((emxArray__common *)I, i1, sizeof(unsigned char));
  i1 = inputImage->size[0] * inputImage->size[1] * inputImage->size[2];
  inputImage->size[0] = X->size[0];
  inputImage->size[1] = X->size[1];
  inputImage->size[2] = X->size[2];
  emxEnsureCapacity((emxArray__common *)inputImage, i1, sizeof(unsigned char));
  loop_ub = X->size[0] * X->size[1] * X->size[2];
  for (i1 = 0; i1 < loop_ub; i1++) {
    inputImage->data[i1] = X->data[i1];
  }

  rgb2gray_tbb_uint8(&inputImage->data[0], (double)(X->size[0] * X->size[1]),
                     &I->data[0]);
  emxFree_uint8_T(&inputImage);
}

/* End of code generation (rgb2gray.c) */
