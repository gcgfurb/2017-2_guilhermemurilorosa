/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * threshold_initialize.c
 *
 * Code generation for function 'threshold_initialize'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "threshold.h"
#include "threshold_initialize.h"

/* Function Definitions */
void threshold_initialize(void)
{
  rt_InitInfAndNaN(8U);
}

/* End of code generation (threshold_initialize.c) */
