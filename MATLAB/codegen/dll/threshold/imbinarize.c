/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * imbinarize.c
 *
 * Code generation for function 'imbinarize'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "threshold.h"
#include "imbinarize.h"
#include "threshold_emxutil.h"
#include "isfinite.h"
#include "imhist.h"

/* Function Definitions */
void imbinarize(const emxArray_uint8_T *I, emxArray_boolean_T *BW)
{
  double sigma_b_squared[256];
  double num_elems;
  int k;
  double omega[256];
  double mu[256];
  double maxval;
  double num_maxval;
  int loop_ub;
  imhist(I, sigma_b_squared);
  num_elems = 0.0;
  for (k = 0; k < 256; k++) {
    num_elems += sigma_b_squared[k];
  }

  omega[0] = sigma_b_squared[0] / num_elems;
  mu[0] = sigma_b_squared[0] / num_elems;
  for (k = 0; k < 255; k++) {
    num_maxval = sigma_b_squared[k + 1] / num_elems;
    omega[k + 1] = omega[k] + num_maxval;
    mu[k + 1] = mu[k] + num_maxval * (2.0 + (double)k);
  }

  maxval = rtMinusInf;
  for (k = 0; k < 256; k++) {
    num_elems = mu[255] * omega[k] - mu[k];
    num_elems = num_elems * num_elems / (omega[k] * (1.0 - omega[k]));
    if (!((maxval > num_elems) || rtIsNaN(num_elems))) {
      maxval = num_elems;
    }

    sigma_b_squared[k] = num_elems;
  }

  if (b_isfinite(maxval)) {
    num_elems = 0.0;
    num_maxval = 0.0;
    for (k = 0; k < 256; k++) {
      num_elems += (double)((1 + k) * (sigma_b_squared[k] == maxval));
      num_maxval += (double)(sigma_b_squared[k] == maxval);
    }

    num_elems /= num_maxval;
    num_elems = (num_elems - 1.0) / 255.0;
  } else {
    num_elems = 0.0;
  }

  k = BW->size[0] * BW->size[1];
  BW->size[0] = I->size[0];
  BW->size[1] = I->size[1];
  emxEnsureCapacity((emxArray__common *)BW, k, sizeof(boolean_T));
  num_elems *= 255.0;
  loop_ub = I->size[0] * I->size[1];
  for (k = 0; k < loop_ub; k++) {
    BW->data[k] = (I->data[k] > num_elems);
  }
}

/* End of code generation (imbinarize.c) */
