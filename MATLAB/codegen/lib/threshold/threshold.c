/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * threshold.c
 *
 * Code generation for function 'threshold'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "threshold.h"
#include "threshold_emxutil.h"
#include "imbinarize.h"
#include "rgb2gray.h"
#include "imread.h"

/* Function Definitions */
void threshold(const char path[7], emxArray_boolean_T *result)
{
  emxArray_uint8_T *original;
  emxArray_uint8_T *gray;
  (void)path;
  emxInit_uint8_T(&original, 3);
  emxInit_uint8_T1(&gray, 2);
  imread(original);
  rgb2gray(original, gray);
  imbinarize(gray, result);
  emxFree_uint8_T(&gray);
  emxFree_uint8_T(&original);
}

/* End of code generation (threshold.c) */
