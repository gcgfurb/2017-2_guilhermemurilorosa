/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * imread.c
 *
 * Code generation for function 'imread'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "threshold.h"
#include "imread.h"
#include "threshold_emxutil.h"
#include "libmwjpegreader.h"

/* Function Definitions */
void imread(emxArray_uint8_T *X)
{
  int i0;
  char libjpegWarnBuffer[200];
  static const char fname[8] = { 'o', 'v', 'o', '.', 'j', 'p', 'g', '\x00' };

  static unsigned char out[38937600];
  static const double outDims[3] = { 3120.0, 4160.0, 3.0 };

  signed char fileStatus;
  signed char libjpegReadDone;
  double libjpegMsgCode;
  signed char errWarnType;
  for (i0 = 0; i0 < 200; i0++) {
    libjpegWarnBuffer[i0] = ' ';
  }

  jpegreader_uint8(fname, out, outDims, 3.0, &fileStatus, &libjpegReadDone,
                   &libjpegMsgCode, libjpegWarnBuffer, &errWarnType);
  if ((fileStatus == -1) || (libjpegReadDone == 0) || (errWarnType == -1)) {
    i0 = X->size[0] * X->size[1] * X->size[2];
    X->size[0] = 0;
    X->size[1] = 0;
    X->size[2] = 0;
    emxEnsureCapacity((emxArray__common *)X, i0, sizeof(unsigned char));
  } else {
    i0 = X->size[0] * X->size[1] * X->size[2];
    X->size[0] = 3120;
    X->size[1] = 4160;
    X->size[2] = 3;
    emxEnsureCapacity((emxArray__common *)X, i0, sizeof(unsigned char));
    for (i0 = 0; i0 < 38937600; i0++) {
      X->data[i0] = out[i0];
    }
  }
}

/* End of code generation (imread.c) */
