/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * isfinite.h
 *
 * Code generation for function 'isfinite'
 *
 */

#ifndef ISFINITE_H
#define ISFINITE_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "threshold_types.h"

/* Function Declarations */
extern boolean_T b_isfinite(double x);

#endif

/* End of code generation (isfinite.h) */
