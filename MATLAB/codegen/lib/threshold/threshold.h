/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * threshold.h
 *
 * Code generation for function 'threshold'
 *
 */

#ifndef THRESHOLD_H
#define THRESHOLD_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "threshold_types.h"

/* Function Declarations */
extern void threshold(const char path[7], emxArray_boolean_T *result);

#endif

/* End of code generation (threshold.h) */
