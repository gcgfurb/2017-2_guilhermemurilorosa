/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * imhist.c
 *
 * Code generation for function 'imhist'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "threshold.h"
#include "imhist.h"
#include "libmwgetnumcores.h"
#include "libmwtbbhist.h"

/* Function Definitions */
void imhist(const emxArray_uint8_T *varargin_1, double yout[256])
{
  double numCores;
  boolean_T useParallel;
  double localBins1[256];
  boolean_T rngFlag;
  double localBins2[256];
  double localBins3[256];
  int i;
  if ((varargin_1->size[0] == 0) || (varargin_1->size[1] == 0)) {
    memset(&yout[0], 0, sizeof(double) << 8);
  } else {
    numCores = 1.0;
    getnumcores(&numCores);
    if ((varargin_1->size[0] * varargin_1->size[1] > 500000) && (numCores > 1.0))
    {
      useParallel = true;
    } else {
      useParallel = false;
    }

    if (useParallel) {
      useParallel = false;
      rngFlag = false;
      tbbhist_uint8(&varargin_1->data[0], (double)(varargin_1->size[0] *
        varargin_1->size[1]), (double)varargin_1->size[0], (double)
                    (varargin_1->size[0] * varargin_1->size[1]) / (double)
                    varargin_1->size[0], yout, 256.0, 256.0, &rngFlag,
                    &useParallel);
    } else {
      memset(&yout[0], 0, sizeof(double) << 8);
      memset(&localBins1[0], 0, sizeof(double) << 8);
      memset(&localBins2[0], 0, sizeof(double) << 8);
      memset(&localBins3[0], 0, sizeof(double) << 8);
      for (i = 0; i + 4 <= varargin_1->size[0] * varargin_1->size[1]; i += 4) {
        localBins1[varargin_1->data[i]]++;
        localBins2[varargin_1->data[i + 1]]++;
        localBins3[varargin_1->data[i + 2]]++;
        yout[varargin_1->data[i + 3]]++;
      }

      while (i + 1 <= varargin_1->size[0] * varargin_1->size[1]) {
        yout[varargin_1->data[i]]++;
        i++;
      }

      for (i = 0; i < 256; i++) {
        yout[i] = ((yout[i] + localBins1[i]) + localBins2[i]) + localBins3[i];
      }
    }
  }
}

/* End of code generation (imhist.c) */
