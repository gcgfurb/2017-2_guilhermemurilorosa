/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * imread.h
 *
 * Code generation for function 'imread'
 *
 */

#ifndef IMREAD_H
#define IMREAD_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "threshold_types.h"

/* Function Declarations */
extern void imread(emxArray_uint8_T *X);

#endif

/* End of code generation (imread.h) */
