/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * imhist.h
 *
 * Code generation for function 'imhist'
 *
 */

#ifndef IMHIST_H
#define IMHIST_H

/* Include files */
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "threshold_types.h"

/* Function Declarations */
extern void imhist(const emxArray_uint8_T *varargin_1, double yout[256]);

#endif

/* End of code generation (imhist.h) */
