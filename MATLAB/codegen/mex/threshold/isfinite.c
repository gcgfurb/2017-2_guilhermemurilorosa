/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * isfinite.c
 *
 * Code generation for function 'isfinite'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "threshold.h"
#include "isfinite.h"

/* Function Definitions */
boolean_T b_isfinite(real_T x)
{
  return (!muDoubleScalarIsInf(x)) && (!muDoubleScalarIsNaN(x));
}

/* End of code generation (isfinite.c) */
