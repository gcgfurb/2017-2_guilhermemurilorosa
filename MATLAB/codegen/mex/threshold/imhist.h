/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * imhist.h
 *
 * Code generation for function 'imhist'
 *
 */

#ifndef IMHIST_H
#define IMHIST_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "covrt.h"
#include "rtwtypes.h"
#include "threshold_types.h"

/* Function Declarations */
extern void imhist(const emlrtStack *sp, const uint8_T varargin_1[12979200],
                   real_T yout[256]);

#endif

/* End of code generation (imhist.h) */
