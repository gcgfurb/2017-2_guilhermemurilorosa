/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * validateattributes.c
 *
 * Code generation for function 'validateattributes'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "threshold.h"
#include "validateattributes.h"

/* Variable Definitions */
static emlrtRTEInfo d_emlrtRTEI = { 154,/* lineNo */
  28,                                  /* colNo */
  "validateattributes",                /* fName */
  "C:\\Program Files\\MATLAB\\R2017a\\toolbox\\eml\\lib\\matlab\\lang\\validateattributes.m"/* pName */
};

static emlrtRTEInfo e_emlrtRTEI = { 149,/* lineNo */
  28,                                  /* colNo */
  "validateattributes",                /* fName */
  "C:\\Program Files\\MATLAB\\R2017a\\toolbox\\eml\\lib\\matlab\\lang\\validateattributes.m"/* pName */
};

/* Function Definitions */
void validateattributes(const emlrtStack *sp, const real_T a[256])
{
  boolean_T p;
  int32_T k;
  boolean_T exitg1;
  p = true;
  k = 0;
  exitg1 = false;
  while ((!exitg1) && (k < 256)) {
    if ((!muDoubleScalarIsInf(a[k])) && (!muDoubleScalarIsNaN(a[k]))) {
      k++;
    } else {
      p = false;
      exitg1 = true;
    }
  }

  if (!p) {
    emlrtErrMsgIdAndExplicitTxt(sp, &d_emlrtRTEI,
      "MATLAB:otsuthresh:expectedFinite", 29, "Expected COUNTS to be finite.");
  }

  p = true;
  k = 0;
  exitg1 = false;
  while ((!exitg1) && (k < 256)) {
    if (!(a[k] < 0.0)) {
      k++;
    } else {
      p = false;
      exitg1 = true;
    }
  }

  if (!p) {
    emlrtErrMsgIdAndExplicitTxt(sp, &e_emlrtRTEI,
      "MATLAB:otsuthresh:expectedNonnegative", 34,
      "Expected COUNTS to be nonnegative.");
  }
}

/* End of code generation (validateattributes.c) */
