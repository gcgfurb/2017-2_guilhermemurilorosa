/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * threshold_types.h
 *
 * Code generation for function 'threshold'
 *
 */

#ifndef THRESHOLD_TYPES_H
#define THRESHOLD_TYPES_H

/* Include files */
#include "rtwtypes.h"

/* Type Definitions */
#ifndef typedef_thresholdStackData
#define typedef_thresholdStackData

typedef struct {
  struct {
    uint8_T original[38937600];
    uint8_T gray[12979200];
  } f0;
} thresholdStackData;

#endif                                 /*typedef_thresholdStackData*/
#endif

/* End of code generation (threshold_types.h) */
