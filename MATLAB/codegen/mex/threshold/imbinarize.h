/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * imbinarize.h
 *
 * Code generation for function 'imbinarize'
 *
 */

#ifndef IMBINARIZE_H
#define IMBINARIZE_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "covrt.h"
#include "rtwtypes.h"
#include "threshold_types.h"

/* Function Declarations */
extern void imbinarize(const emlrtStack *sp, const uint8_T I[12979200],
  boolean_T BW[12979200]);

#endif

/* End of code generation (imbinarize.h) */
