/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * warning.h
 *
 * Code generation for function 'warning'
 *
 */

#ifndef WARNING_H
#define WARNING_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "covrt.h"
#include "rtwtypes.h"
#include "threshold_types.h"

/* Function Declarations */
extern void b_warning(const emlrtStack *sp);
extern void c_warning(const emlrtStack *sp);
extern void warning(const emlrtStack *sp, real_T varargin_1, const char_T
                    varargin_2[200]);

#endif

/* End of code generation (warning.h) */
