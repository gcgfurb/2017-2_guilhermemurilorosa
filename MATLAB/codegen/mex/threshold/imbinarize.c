/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * imbinarize.c
 *
 * Code generation for function 'imbinarize'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "threshold.h"
#include "imbinarize.h"
#include "otsuthresh.h"
#include "imhist.h"

/* Variable Definitions */
static emlrtRSInfo g_emlrtRSI = { 70,  /* lineNo */
  "imbinarize",                        /* fcnName */
  "C:\\Program Files\\MATLAB\\R2017a\\toolbox\\images\\images\\eml\\imbinarize.m"/* pathName */
};

static emlrtRSInfo h_emlrtRSI = { 104, /* lineNo */
  "imbinarize",                        /* fcnName */
  "C:\\Program Files\\MATLAB\\R2017a\\toolbox\\images\\images\\eml\\imbinarize.m"/* pathName */
};

/* Function Definitions */
void imbinarize(const emlrtStack *sp, const uint8_T I[12979200], boolean_T BW
                [12979200])
{
  real_T dv0[256];
  real_T t;
  int32_T i1;
  emlrtStack st;
  emlrtStack b_st;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &g_emlrtRSI;
  b_st.prev = &st;
  b_st.tls = st.tls;
  b_st.site = &h_emlrtRSI;
  imhist(&b_st, I, dv0);
  b_st.site = &h_emlrtRSI;
  t = otsuthresh(&b_st, dv0);
  t *= 255.0;
  for (i1 = 0; i1 < 12979200; i1++) {
    BW[i1] = (I[i1] > t);
  }
}

/* End of code generation (imbinarize.c) */
