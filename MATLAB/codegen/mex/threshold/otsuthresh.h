/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * otsuthresh.h
 *
 * Code generation for function 'otsuthresh'
 *
 */

#ifndef OTSUTHRESH_H
#define OTSUTHRESH_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "covrt.h"
#include "rtwtypes.h"
#include "threshold_types.h"

/* Function Declarations */
extern real_T otsuthresh(const emlrtStack *sp, const real_T counts[256]);

#ifdef __WATCOMC__

#pragma aux otsuthresh value [8087];

#endif
#endif

/* End of code generation (otsuthresh.h) */
