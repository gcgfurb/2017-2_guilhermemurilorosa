/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * threshold.h
 *
 * Code generation for function 'threshold'
 *
 */

#ifndef THRESHOLD_H
#define THRESHOLD_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "covrt.h"
#include "rtwtypes.h"
#include "threshold_types.h"

/* Function Declarations */
extern void threshold(thresholdStackData *SD, const emlrtStack *sp, const char_T
                      path[7], boolean_T result[12979200]);

#endif

/* End of code generation (threshold.h) */
