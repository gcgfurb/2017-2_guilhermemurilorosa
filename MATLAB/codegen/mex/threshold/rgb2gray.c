/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * rgb2gray.c
 *
 * Code generation for function 'rgb2gray'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "threshold.h"
#include "rgb2gray.h"
#include "libmwrgb2gray_tbb.h"

/* Function Definitions */
void rgb2gray(const uint8_T X[38937600], uint8_T I[12979200])
{
  rgb2gray_tbb_uint8(X, 1.29792E+7, I);
}

/* End of code generation (rgb2gray.c) */
