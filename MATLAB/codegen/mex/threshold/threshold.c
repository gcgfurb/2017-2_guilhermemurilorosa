/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * threshold.c
 *
 * Code generation for function 'threshold'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "threshold.h"
#include "rgb2gray.h"
#include "imbinarize.h"
#include "imread.h"
#include "threshold_data.h"

/* Variable Definitions */
static emlrtRSInfo emlrtRSI = { 2,     /* lineNo */
  "threshold",                         /* fcnName */
  "C:\\Users\\GuilhermeMurilo\\Documents\\guilhermemurilorosa\\MATLAB\\threshold.m"/* pathName */
};

static emlrtRSInfo c_emlrtRSI = { 4,   /* lineNo */
  "threshold",                         /* fcnName */
  "C:\\Users\\GuilhermeMurilo\\Documents\\guilhermemurilorosa\\MATLAB\\threshold.m"/* pathName */
};

/* Function Definitions */
void threshold(thresholdStackData *SD, const emlrtStack *sp, const char_T path[7],
               boolean_T result[12979200])
{
  emlrtStack st;
  (void)path;
  st.prev = sp;
  st.tls = sp->tls;
  covrtLogFcn(&emlrtCoverageInstance, 0U, 0U);
  covrtLogBasicBlock(&emlrtCoverageInstance, 0U, 0U);
  st.site = &emlrtRSI;
  imread(&st, SD->f0.original);
  rgb2gray(SD->f0.original, SD->f0.gray);
  st.site = &c_emlrtRSI;
  imbinarize(&st, SD->f0.gray, result);
}

/* End of code generation (threshold.c) */
