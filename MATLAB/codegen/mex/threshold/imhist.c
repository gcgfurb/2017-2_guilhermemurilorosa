/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * imhist.c
 *
 * Code generation for function 'imhist'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "threshold.h"
#include "imhist.h"
#include "warning.h"
#include "libmwgetnumcores.h"
#include "libmwtbbhist.h"

/* Variable Definitions */
static emlrtRSInfo i_emlrtRSI = { 121, /* lineNo */
  "imhist",                            /* fcnName */
  "C:\\Program Files\\MATLAB\\R2017a\\toolbox\\images\\images\\eml\\imhist.m"/* pathName */
};

static emlrtRSInfo l_emlrtRSI = { 185, /* lineNo */
  "imhist",                            /* fcnName */
  "C:\\Program Files\\MATLAB\\R2017a\\toolbox\\images\\images\\eml\\imhist.m"/* pathName */
};

static emlrtRSInfo m_emlrtRSI = { 410, /* lineNo */
  "imhist",                            /* fcnName */
  "C:\\Program Files\\MATLAB\\R2017a\\toolbox\\images\\images\\eml\\imhist.m"/* pathName */
};

static emlrtRSInfo n_emlrtRSI = { 414, /* lineNo */
  "imhist",                            /* fcnName */
  "C:\\Program Files\\MATLAB\\R2017a\\toolbox\\images\\images\\eml\\imhist.m"/* pathName */
};

static emlrtBCInfo emlrtBCI = { 1,     /* iFirst */
  12979200,                            /* iLast */
  619,                                 /* lineNo */
  44,                                  /* colNo */
  "",                                  /* aName */
  "imhist",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2017a\\toolbox\\images\\images\\eml\\imhist.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo b_emlrtBCI = { 1,   /* iFirst */
  12979200,                            /* iLast */
  618,                                 /* lineNo */
  44,                                  /* colNo */
  "",                                  /* aName */
  "imhist",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2017a\\toolbox\\images\\images\\eml\\imhist.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo c_emlrtBCI = { 1,   /* iFirst */
  12979200,                            /* iLast */
  617,                                 /* lineNo */
  44,                                  /* colNo */
  "",                                  /* aName */
  "imhist",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2017a\\toolbox\\images\\images\\eml\\imhist.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo d_emlrtBCI = { 1,   /* iFirst */
  12979200,                            /* iLast */
  616,                                 /* lineNo */
  44,                                  /* colNo */
  "",                                  /* aName */
  "imhist",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2017a\\toolbox\\images\\images\\eml\\imhist.m",/* pName */
  0                                    /* checkKind */
};

/* Function Definitions */
void imhist(const emlrtStack *sp, const uint8_T varargin_1[12979200], real_T
            yout[256])
{
  real_T numCores;
  boolean_T nanFlag;
  real_T localBins1[256];
  boolean_T rngFlag;
  real_T localBins2[256];
  real_T localBins3[256];
  int32_T i;
  emlrtStack st;
  emlrtStack b_st;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &i_emlrtRSI;
  b_st.prev = &st;
  b_st.tls = st.tls;
  numCores = 1.0;
  getnumcores(&numCores);
  if (numCores > 1.0) {
    nanFlag = false;
    rngFlag = false;
    tbbhist_uint8(varargin_1, 1.29792E+7, 3120.0, 4160.0, yout, 256.0, 256.0,
                  &rngFlag, &nanFlag);
  } else {
    b_st.site = &l_emlrtRSI;
    memset(&yout[0], 0, sizeof(real_T) << 8);
    memset(&localBins1[0], 0, sizeof(real_T) << 8);
    memset(&localBins2[0], 0, sizeof(real_T) << 8);
    memset(&localBins3[0], 0, sizeof(real_T) << 8);
    for (i = 1; i + 3 <= 12979200; i += 4) {
      if (!((i >= 1) && (i <= 12979200))) {
        emlrtDynamicBoundsCheckR2012b(i, 1, 12979200, &d_emlrtBCI, &b_st);
      }

      if (!((i + 1 >= 1) && (i + 1 <= 12979200))) {
        emlrtDynamicBoundsCheckR2012b(i + 1, 1, 12979200, &c_emlrtBCI, &b_st);
      }

      if (!((i + 2 >= 1) && (i + 2 <= 12979200))) {
        emlrtDynamicBoundsCheckR2012b(i + 2, 1, 12979200, &b_emlrtBCI, &b_st);
      }

      if (!((i + 3 >= 1) && (i + 3 <= 12979200))) {
        emlrtDynamicBoundsCheckR2012b(i + 3, 1, 12979200, &emlrtBCI, &b_st);
      }

      localBins1[varargin_1[i - 1]]++;
      localBins2[varargin_1[i]]++;
      localBins3[varargin_1[i + 1]]++;
      yout[varargin_1[i + 2]]++;
    }

    while (i <= 12979200) {
      yout[varargin_1[i - 1]]++;
      i++;
    }

    for (i = 0; i < 256; i++) {
      yout[i] = ((yout[i] + localBins1[i]) + localBins2[i]) + localBins3[i];
    }

    rngFlag = false;
    nanFlag = false;
  }

  if (rngFlag) {
    b_st.site = &m_emlrtRSI;
    b_warning(&b_st);
  }

  if (nanFlag) {
    b_st.site = &n_emlrtRSI;
    c_warning(&b_st);
  }
}

/* End of code generation (imhist.c) */
