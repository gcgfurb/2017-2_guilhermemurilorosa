/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * _coder_threshold_api.c
 *
 * Code generation for function '_coder_threshold_api'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "threshold.h"
#include "_coder_threshold_api.h"
#include "threshold_data.h"

/* Function Declarations */
static void b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, char_T y[7]);
static void c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, char_T ret[7]);
static void emlrt_marshallIn(const emlrtStack *sp, const mxArray *path, const
  char_T *identifier, char_T y[7]);
static const mxArray *emlrt_marshallOut(const boolean_T u[12979200]);

/* Function Definitions */
static void b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, char_T y[7])
{
  c_emlrt_marshallIn(sp, emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static void c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, char_T ret[7])
{
  static const int32_T dims[2] = { 1, 7 };

  emlrtCheckBuiltInR2012b(sp, msgId, src, "char", false, 2U, dims);
  emlrtImportCharArrayR2015b(sp, src, ret, 7);
  emlrtDestroyArray(&src);
}

static void emlrt_marshallIn(const emlrtStack *sp, const mxArray *path, const
  char_T *identifier, char_T y[7])
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  b_emlrt_marshallIn(sp, emlrtAlias(path), &thisId, y);
  emlrtDestroyArray(&path);
}

static const mxArray *emlrt_marshallOut(const boolean_T u[12979200])
{
  const mxArray *y;
  const mxArray *m3;
  static const int32_T iv10[2] = { 0, 0 };

  static const int32_T iv11[2] = { 3120, 4160 };

  y = NULL;
  m3 = emlrtCreateLogicalArray(2, iv10);
  mxSetData((mxArray *)m3, (void *)&u[0]);
  emlrtSetDimensions((mxArray *)m3, iv11, 2);
  emlrtAssign(&y, m3);
  return y;
}

void threshold_api(thresholdStackData *SD, const mxArray * const prhs[1], const
                   mxArray *plhs[1])
{
  boolean_T (*result)[12979200];
  char_T path[7];
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;
  result = (boolean_T (*)[12979200])mxMalloc(sizeof(boolean_T [12979200]));

  /* Marshall function inputs */
  emlrt_marshallIn(&st, emlrtAliasP(prhs[0]), "path", path);

  /* Invoke the target function */
  threshold(SD, &st, path, *result);

  /* Marshall function outputs */
  plhs[0] = emlrt_marshallOut(*result);
}

/* End of code generation (_coder_threshold_api.c) */
