/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * _coder_threshold_mex.c
 *
 * Code generation for function '_coder_threshold_mex'
 *
 */

/* Include files */
#include "threshold.h"
#include "_coder_threshold_mex.h"
#include "threshold_terminate.h"
#include "_coder_threshold_api.h"
#include "threshold_initialize.h"
#include "threshold_data.h"

/* Function Declarations */
static void threshold_mexFunction(thresholdStackData *SD, int32_T nlhs, mxArray *
  plhs[1], int32_T nrhs, const mxArray *prhs[1]);

/* Function Definitions */
static void threshold_mexFunction(thresholdStackData *SD, int32_T nlhs, mxArray *
  plhs[1], int32_T nrhs, const mxArray *prhs[1])
{
  int32_T n;
  const mxArray *inputs[1];
  const mxArray *outputs[1];
  int32_T b_nlhs;
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;

  /* Check for proper number of arguments. */
  if (nrhs != 1) {
    emlrtErrMsgIdAndTxt(&st, "EMLRT:runTime:WrongNumberOfInputs", 5, 12, 1, 4, 9,
                        "threshold");
  }

  if (nlhs > 1) {
    emlrtErrMsgIdAndTxt(&st, "EMLRT:runTime:TooManyOutputArguments", 3, 4, 9,
                        "threshold");
  }

  /* Temporary copy for mex inputs. */
  for (n = 0; n < nrhs; n++) {
    inputs[n] = prhs[n];
    if (*emlrtBreakCheckR2012bFlagVar != 0) {
      emlrtBreakCheckR2012b(&st);
    }
  }

  /* Call the function. */
  threshold_api(SD, inputs, outputs);

  /* Copy over outputs to the caller. */
  if (nlhs < 1) {
    b_nlhs = 1;
  } else {
    b_nlhs = nlhs;
  }

  emlrtReturnArrays(b_nlhs, plhs, outputs);

  /* Module termination. */
  threshold_terminate();
}

void mexFunction(int32_T nlhs, mxArray *plhs[], int32_T nrhs, const mxArray
                 *prhs[])
{
  thresholdStackData *thresholdStackDataGlobal = NULL;
  thresholdStackDataGlobal = (thresholdStackData *)mxCalloc(1, 1U * sizeof
    (thresholdStackData));
  mexAtExit(threshold_atexit);

  /* Initialize the memory manager. */
  /* Module initialization. */
  threshold_initialize();

  /* Dispatch the entry-point. */
  threshold_mexFunction(thresholdStackDataGlobal, nlhs, plhs, nrhs, prhs);
  mxFree(thresholdStackDataGlobal);
}

emlrtCTX mexFunctionCreateRootTLS(void)
{
  emlrtCreateRootTLS(&emlrtRootTLSGlobal, &emlrtContextGlobal, NULL, 1);
  return emlrtRootTLSGlobal;
}

/* End of code generation (_coder_threshold_mex.c) */
