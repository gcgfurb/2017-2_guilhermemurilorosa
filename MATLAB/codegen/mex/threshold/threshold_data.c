/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * threshold_data.c
 *
 * Code generation for function 'threshold_data'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "threshold.h"
#include "threshold_data.h"

/* Variable Definitions */
emlrtCTX emlrtRootTLSGlobal = NULL;
const volatile char_T *emlrtBreakCheckR2012bFlagVar = NULL;
covrtInstance emlrtCoverageInstance;
emlrtContext emlrtContextGlobal = { true,/* bFirstTime */
  false,                               /* bInitialized */
  131450U,                             /* fVersionInfo */
  NULL,                                /* fErrorFunction */
  "threshold",                         /* fFunctionName */
  NULL,                                /* fRTCallStack */
  false,                               /* bDebugMode */
  { 1858410525U, 2505464270U, 328108647U, 1256672073U },/* fSigWrd */
  NULL                                 /* fSigMem */
};

emlrtRSInfo b_emlrtRSI = { 3,          /* lineNo */
  "threshold",                         /* fcnName */
  "C:\\Users\\GuilhermeMurilo\\Documents\\guilhermemurilorosa\\MATLAB\\threshold.m"/* pathName */
};

emlrtRSInfo d_emlrtRSI = { 105,        /* lineNo */
  "imread",                            /* fcnName */
  "C:\\Program Files\\MATLAB\\R2017a\\toolbox\\eml\\lib\\matlab\\imagesci\\imread.m"/* pathName */
};

emlrtRSInfo f_emlrtRSI = { 46,         /* lineNo */
  "rgb2gray",                          /* fcnName */
  "C:\\Program Files\\MATLAB\\R2017a\\toolbox\\eml\\lib\\matlab\\specfun\\rgb2gray.m"/* pathName */
};

emlrtRSInfo j_emlrtRSI = { 154,        /* lineNo */
  "imhist",                            /* fcnName */
  "C:\\Program Files\\MATLAB\\R2017a\\toolbox\\images\\images\\eml\\imhist.m"/* pathName */
};

emlrtRSInfo k_emlrtRSI = { 174,        /* lineNo */
  "imhist",                            /* fcnName */
  "C:\\Program Files\\MATLAB\\R2017a\\toolbox\\images\\images\\eml\\imhist.m"/* pathName */
};

emlrtRSInfo p_emlrtRSI = { 71,         /* lineNo */
  "otsuthresh",                        /* fcnName */
  "C:\\Program Files\\MATLAB\\R2017a\\toolbox\\images\\images\\eml\\otsuthresh.m"/* pathName */
};

emlrtRSInfo q_emlrtRSI = { 37,         /* lineNo */
  "mpower",                            /* fcnName */
  "C:\\Program Files\\MATLAB\\R2017a\\toolbox\\eml\\lib\\matlab\\ops\\mpower.m"/* pathName */
};

emlrtRSInfo r_emlrtRSI = { 49,         /* lineNo */
  "power",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2017a\\toolbox\\eml\\lib\\matlab\\ops\\power.m"/* pathName */
};

/* End of code generation (threshold_data.c) */
