/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * otsuthresh.c
 *
 * Code generation for function 'otsuthresh'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "threshold.h"
#include "otsuthresh.h"
#include "mpower.h"
#include "isfinite.h"
#include "validateattributes.h"

/* Variable Definitions */
static emlrtRSInfo o_emlrtRSI = { 37,  /* lineNo */
  "otsuthresh",                        /* fcnName */
  "C:\\Program Files\\MATLAB\\R2017a\\toolbox\\images\\images\\eml\\otsuthresh.m"/* pathName */
};

/* Function Definitions */
real_T otsuthresh(const emlrtStack *sp, const real_T counts[256])
{
  real_T t;
  real_T num_elems;
  int32_T k;
  real_T omega[256];
  real_T mu[256];
  real_T maxval;
  real_T num_maxval;
  real_T sigma_b_squared[256];
  emlrtStack st;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &o_emlrtRSI;
  validateattributes(&st, counts);
  num_elems = 0.0;
  for (k = 0; k < 256; k++) {
    num_elems += counts[k];
  }

  omega[0] = counts[0] / num_elems;
  mu[0] = counts[0] / num_elems;
  for (k = 0; k < 255; k++) {
    num_maxval = counts[k + 1] / num_elems;
    omega[k + 1] = omega[k] + num_maxval;
    mu[k + 1] = mu[k] + num_maxval * (2.0 + (real_T)k);
  }

  maxval = rtMinusInf;
  for (k = 0; k < 256; k++) {
    num_elems = mpower(mu[255] * omega[k] - mu[k]);
    num_elems /= omega[k] * (1.0 - omega[k]);
    maxval = muDoubleScalarMax(maxval, num_elems);
    sigma_b_squared[k] = num_elems;
  }

  if (b_isfinite(maxval)) {
    num_elems = 0.0;
    num_maxval = 0.0;
    for (k = 0; k < 256; k++) {
      num_elems += (real_T)((1 + k) * (sigma_b_squared[k] == maxval));
      num_maxval += (real_T)(sigma_b_squared[k] == maxval);
    }

    num_elems /= num_maxval;
    t = (num_elems - 1.0) / 255.0;
  } else {
    t = 0.0;
  }

  return t;
}

/* End of code generation (otsuthresh.c) */
