@echo off
set MATLAB=C:\PROGRA~1\MATLAB\R2017a
set MATLAB_ARCH=win64
set MATLAB_BIN="C:\Program Files\MATLAB\R2017a\bin"
set ENTRYPOINT=mexFunction
set OUTDIR=.\
set LIB_NAME=threshold_mex
set MEX_NAME=threshold_mex
set MEX_EXT=.mexw64
call "C:\PROGRA~1\MATLAB\R2017a\sys\lcc64\lcc64\mex\lcc64opts.bat"
echo # Make settings for threshold > threshold_mex.mki
echo COMPILER=%COMPILER%>> threshold_mex.mki
echo COMPFLAGS=%COMPFLAGS%>> threshold_mex.mki
echo OPTIMFLAGS=%OPTIMFLAGS%>> threshold_mex.mki
echo DEBUGFLAGS=%DEBUGFLAGS%>> threshold_mex.mki
echo LINKER=%LINKER%>> threshold_mex.mki
echo LINKFLAGS=%LINKFLAGS%>> threshold_mex.mki
echo LINKOPTIMFLAGS=%LINKOPTIMFLAGS%>> threshold_mex.mki
echo LINKDEBUGFLAGS=%LINKDEBUGFLAGS%>> threshold_mex.mki
echo MATLAB_ARCH=%MATLAB_ARCH%>> threshold_mex.mki
echo BORLAND=%BORLAND%>> threshold_mex.mki
echo OMPFLAGS= >> threshold_mex.mki
echo OMPLINKFLAGS= >> threshold_mex.mki
echo EMC_COMPILER=lcc64>> threshold_mex.mki
echo EMC_CONFIG=optim>> threshold_mex.mki
"C:\Program Files\MATLAB\R2017a\bin\win64\gmake" -B -f threshold_mex.mk
