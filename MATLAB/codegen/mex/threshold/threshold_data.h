/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * threshold_data.h
 *
 * Code generation for function 'threshold_data'
 *
 */

#ifndef THRESHOLD_DATA_H
#define THRESHOLD_DATA_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "covrt.h"
#include "rtwtypes.h"
#include "threshold_types.h"

/* Variable Declarations */
extern emlrtCTX emlrtRootTLSGlobal;
extern const volatile char_T *emlrtBreakCheckR2012bFlagVar;
extern covrtInstance emlrtCoverageInstance;
extern emlrtContext emlrtContextGlobal;
extern emlrtRSInfo b_emlrtRSI;
extern emlrtRSInfo d_emlrtRSI;
extern emlrtRSInfo f_emlrtRSI;
extern emlrtRSInfo j_emlrtRSI;
extern emlrtRSInfo k_emlrtRSI;
extern emlrtRSInfo p_emlrtRSI;
extern emlrtRSInfo q_emlrtRSI;
extern emlrtRSInfo r_emlrtRSI;

#endif

/* End of code generation (threshold_data.h) */
