/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * mpower.c
 *
 * Code generation for function 'mpower'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "threshold.h"
#include "mpower.h"

/* Function Definitions */
real_T mpower(real_T a)
{
  return a * a;
}

/* End of code generation (mpower.c) */
