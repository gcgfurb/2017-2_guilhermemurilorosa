/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * rgb2gray.h
 *
 * Code generation for function 'rgb2gray'
 *
 */

#ifndef RGB2GRAY_H
#define RGB2GRAY_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "covrt.h"
#include "rtwtypes.h"
#include "threshold_types.h"

/* Function Declarations */
extern void rgb2gray(const uint8_T X[38937600], uint8_T I[12979200]);

#endif

/* End of code generation (rgb2gray.h) */
