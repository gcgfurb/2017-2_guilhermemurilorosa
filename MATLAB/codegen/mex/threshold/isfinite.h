/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * isfinite.h
 *
 * Code generation for function 'isfinite'
 *
 */

#ifndef ISFINITE_H
#define ISFINITE_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "covrt.h"
#include "rtwtypes.h"
#include "threshold_types.h"

/* Function Declarations */
extern boolean_T b_isfinite(real_T x);

#endif

/* End of code generation (isfinite.h) */
