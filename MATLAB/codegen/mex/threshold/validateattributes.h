/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * validateattributes.h
 *
 * Code generation for function 'validateattributes'
 *
 */

#ifndef VALIDATEATTRIBUTES_H
#define VALIDATEATTRIBUTES_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "covrt.h"
#include "rtwtypes.h"
#include "threshold_types.h"

/* Function Declarations */
extern void validateattributes(const emlrtStack *sp, const real_T a[256]);

#endif

/* End of code generation (validateattributes.h) */
