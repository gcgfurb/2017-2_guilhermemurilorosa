/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * imread.c
 *
 * Code generation for function 'imread'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "threshold.h"
#include "imread.h"
#include "warning.h"
#include "libmwjpegreader.h"

/* Variable Definitions */
static emlrtRSInfo e_emlrtRSI = { 149, /* lineNo */
  "imread",                            /* fcnName */
  "C:\\Program Files\\MATLAB\\R2017a\\toolbox\\eml\\lib\\matlab\\imagesci\\imread.m"/* pathName */
};

static emlrtRTEInfo emlrtRTEI = { 117, /* lineNo */
  9,                                   /* colNo */
  "imread",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2017a\\toolbox\\eml\\lib\\matlab\\imagesci\\imread.m"/* pName */
};

static emlrtRTEInfo b_emlrtRTEI = { 120,/* lineNo */
  9,                                   /* colNo */
  "imread",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2017a\\toolbox\\eml\\lib\\matlab\\imagesci\\imread.m"/* pName */
};

static emlrtRTEInfo c_emlrtRTEI = { 125,/* lineNo */
  9,                                   /* colNo */
  "imread",                            /* fName */
  "C:\\Program Files\\MATLAB\\R2017a\\toolbox\\eml\\lib\\matlab\\imagesci\\imread.m"/* pName */
};

/* Function Definitions */
void imread(const emlrtStack *sp, uint8_T X[38937600])
{
  int32_T i0;
  char_T libjpegWarnBuffer[200];
  static const char_T fname[8] = { 'o', 'v', 'o', '.', 'j', 'p', 'g', '\x00' };

  static const real_T outDims[3] = { 3120.0, 4160.0, 3.0 };

  int8_T fileStatus;
  int8_T libjpegReadDone;
  real_T libjpegMsgCode;
  int8_T errWarnType;
  emlrtStack st;
  st.prev = sp;
  st.tls = sp->tls;
  for (i0 = 0; i0 < 200; i0++) {
    libjpegWarnBuffer[i0] = ' ';
  }

  jpegreader_uint8(fname, X, outDims, 3.0, &fileStatus, &libjpegReadDone,
                   &libjpegMsgCode, libjpegWarnBuffer, &errWarnType);
  if ((fileStatus == -1) || (libjpegReadDone == 0) || (errWarnType == -1)) {
    if (fileStatus == -1) {
      emlrtErrorWithMessageIdR2012b(sp, &emlrtRTEI,
        "MATLAB:imagesci:imread:codegenFileOpenError", 3, 4, 7, "ovo.jpg");
    }

    if (errWarnType == -1) {
      emlrtErrorWithMessageIdR2012b(sp, &b_emlrtRTEI,
        "MATLAB:imagesci:jpg:unhandledLibraryError", 5, 6, 8.0, 4, 200,
        libjpegWarnBuffer);
    }

    if (libjpegReadDone != 1) {
      emlrtErrorWithMessageIdR2012b(sp, &c_emlrtRTEI,
        "MATLAB:imagesci:jpg:unhandledLibraryError", 5, 6, 8.0, 4, 200,
        libjpegWarnBuffer);
    }
  }

  if (errWarnType == -2) {
    st.site = &e_emlrtRSI;
    warning(&st, 8.0, libjpegWarnBuffer);
  }
}

/* End of code generation (imread.c) */
