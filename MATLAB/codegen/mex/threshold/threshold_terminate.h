/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * threshold_terminate.h
 *
 * Code generation for function 'threshold_terminate'
 *
 */

#ifndef THRESHOLD_TERMINATE_H
#define THRESHOLD_TERMINATE_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "covrt.h"
#include "rtwtypes.h"
#include "threshold_types.h"

/* Function Declarations */
extern void threshold_atexit(void);
extern void threshold_terminate(void);

#endif

/* End of code generation (threshold_terminate.h) */
