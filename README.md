# README #

EGG AVALIATOR: OVOSCOPIA VIA SMARTPHONE

Passo a passo: 
1 - Read the RGB (colored) image in from user. 
2 - Convert image from (RGB) colored to gray image. 
3 - Threshold the image (convert gray image to binary image). 
4 - Invert the binary image (in order to speed up the time of processing). 
5 - Find the boundaries concentrate. 
6 - Determine shapes properties (ratio of dimensions, roundness). 
7 - Classify shapes according to its properties.

Input 
----- 
RGB image have the shapes to recognize.

Output 
------ 
The RGB image with shapes recognized and labeled.