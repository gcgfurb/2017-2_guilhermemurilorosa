package furb.com.br.eggevaluation;

import android.os.Environment;

import org.bytedeco.javacpp.Loader;
import org.bytedeco.javacpp.annotation.Const;
import org.bytedeco.javacpp.helper.opencv_core.CvArr;
import org.bytedeco.javacpp.opencv_imgproc;

import static org.bytedeco.javacpp.helper.opencv_core.CV_RGB;
import static org.bytedeco.javacpp.opencv_core.CvContour;
import static org.bytedeco.javacpp.opencv_core.CvMemStorage;
import static org.bytedeco.javacpp.opencv_core.CvPoint;
import static org.bytedeco.javacpp.opencv_core.CvRect;
import static org.bytedeco.javacpp.opencv_core.CvScalar;
import static org.bytedeco.javacpp.opencv_core.CvSeq;
import static org.bytedeco.javacpp.opencv_core.IPL_DEPTH_8U;
import static org.bytedeco.javacpp.opencv_core.IplImage;
import static org.bytedeco.javacpp.opencv_core.Mat;
import static org.bytedeco.javacpp.opencv_core.Rect;
import static org.bytedeco.javacpp.opencv_core.cvAvg;
import static org.bytedeco.javacpp.opencv_core.cvCreateImage;
import static org.bytedeco.javacpp.opencv_core.cvGet2D;
import static org.bytedeco.javacpp.opencv_core.cvGetSize;
import static org.bytedeco.javacpp.opencv_core.cvPoint;
import static org.bytedeco.javacpp.opencv_core.cvSplit;
import static org.bytedeco.javacpp.opencv_imgcodecs.cvSaveImage;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_imgproc.COLOR_BGR2HSV;
import static org.bytedeco.javacpp.opencv_imgproc.CV_CHAIN_APPROX_NONE;
import static org.bytedeco.javacpp.opencv_imgproc.CV_FILLED;
import static org.bytedeco.javacpp.opencv_imgproc.CV_RETR_CCOMP;
import static org.bytedeco.javacpp.opencv_imgproc.THRESH_BINARY;
import static org.bytedeco.javacpp.opencv_imgproc.cvBoundingRect;
import static org.bytedeco.javacpp.opencv_imgproc.cvContourArea;
import static org.bytedeco.javacpp.opencv_imgproc.cvCvtColor;
import static org.bytedeco.javacpp.opencv_imgproc.cvDilate;
import static org.bytedeco.javacpp.opencv_imgproc.cvDrawContours;
import static org.bytedeco.javacpp.opencv_imgproc.cvErode;
import static org.bytedeco.javacpp.opencv_imgproc.cvFindContours;
import static org.bytedeco.javacpp.opencv_imgproc.cvRectangle;
import static org.bytedeco.javacpp.opencv_imgproc.cvtColor;
import static org.bytedeco.javacpp.opencv_imgproc.threshold;

abstract class MyCV {

    protected static final String TAG = "EggEvaluation";
    protected static final String IMG_DIR = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/" + TAG + "/";
    protected static String imgPath = IMG_DIR + "img5.jpg";
    protected int threshold = 0;

    protected Mat imgRead() {
        return imread(imgPath);
    }

    protected IplImage getRedChannel(IplImage src) {
        IplImage[] imgs = rgbSplit(src);
        try {
            return imgs[0];
        } finally {
            imgs[1].close();
            imgs[2].close();
        }
    }

    protected IplImage getGreenChannel(IplImage src) {
        IplImage[] imgs = rgbSplit(src);
        try {
            return imgs[1];
        } finally {
            imgs[0].close();
            imgs[2].close();
        }
    }

    protected IplImage getBlueChannel(IplImage src) {
        IplImage[] imgs = rgbSplit(src);
        try {
            return imgs[2];
        } finally {
            imgs[0].close();
            imgs[1].close();
        }
    }

    protected IplImage[] rgbSplit(IplImage src) {
        IplImage redChannel = cvCreateImage(cvGetSize(src), IPL_DEPTH_8U, 1);
        IplImage greenChannel = cvCreateImage(cvGetSize(src), IPL_DEPTH_8U, 1);
        IplImage blueChannel = cvCreateImage(cvGetSize(src), IPL_DEPTH_8U, 1);
        cvSplit(src, blueChannel, redChannel, greenChannel, null);
        return new IplImage[]{redChannel, greenChannel, blueChannel};
    }

    protected IplImage getHueChannel(IplImage src) {
        IplImage[] imgs = hsvSplit(src);
        try {
            return imgs[0];
        } finally {
            imgs[1].close();
            imgs[2].close();
        }
    }

    protected IplImage getSaturationChannel(IplImage src) {
        IplImage[] imgs = hsvSplit(src);
        try {
            return imgs[1];
        } finally {
            imgs[0].close();
            imgs[2].close();
        }
    }

    protected IplImage getValueChannel(IplImage src) {
        IplImage[] imgs = hsvSplit(src);
        try {
            return imgs[2];
        } finally {
            imgs[0].close();
            imgs[1].close();
        }
    }

    protected IplImage[] hsvSplit(IplImage src) {
        try (IplImage hsv = cvCreateImage(cvGetSize(src), 8, 3)) {
            cvCvtColor(src, hsv, COLOR_BGR2HSV);
            IplImage hueChannel = cvCreateImage(cvGetSize(src), IPL_DEPTH_8U, 1);
            IplImage saturationChannel = cvCreateImage(cvGetSize(src), IPL_DEPTH_8U, 1);
            IplImage valueChannel = cvCreateImage(cvGetSize(src), IPL_DEPTH_8U, 1);
            cvSplit(hsv, hueChannel, saturationChannel, valueChannel, null);
            return new IplImage[]{hueChannel, saturationChannel, valueChannel};
        }
    }

    protected double getAvgThreshold(Mat src) {
        try (IplImage img = new IplImage(src)) {
            return getAvgThreshold(img);
        }
    }

    protected double getAvgThreshold(IplImage src) {
        if (threshold == 0) {
            try (CvScalar avg = cvAvg(src)) {
                return avg.val(0);
            }
        }
        return threshold;
    }


    protected Mat convertToGray(Mat srcMat) {
        Mat grayMat = srcMat.clone();
        cvtColor(grayMat, grayMat, opencv_imgproc.CV_RGB2GRAY);
        return grayMat;
    }

    protected Mat myThreshold(IplImage grayImg, double threshold) {
        return myThreshold(new Mat(grayImg), threshold);
    }

    protected Mat myThreshold(Mat grayMat, double threshold) {
        threshold(grayMat, grayMat, threshold, 255, THRESH_BINARY);
        return grayMat;
    }

    protected Rect getEggRoiRectangle(Mat binaryMat) {
        try (IplImage contoursImg = new IplImage(binaryMat)) {
            try (CvMemStorage storageCrop = CvMemStorage.create()) {
                try (CvSeq contoursCrop = new CvContour()) {
                    cvFindContours(//
                            contoursImg, //
                            storageCrop, //
                            contoursCrop, //
                            Loader.sizeof(CvContour.class), //
                            CV_RETR_CCOMP, //
                            CV_CHAIN_APPROX_NONE, //
                            new CvPoint(0, 0));

                    try (CvSeq eggContour = findBigContour(contoursCrop)) {
                        try (CvRect eggRectangle = cvBoundingRect(eggContour, 0)) {
                            return new Rect(//
                                    eggRectangle.x(), //
                                    eggRectangle.y(), //
                                    eggRectangle.width(), //
                                    eggRectangle.height());
                        }
                    }
                }
            }
        }
    }

    protected IplImage getEggFilledInWhiteImg(Mat grayCroppedMat, Mat eggFilledInWhiteMat, double threshold) {
        try (Mat grayCroppedBinaryMat = myThreshold(grayCroppedMat, threshold - 20); IplImage grayCroppedBinaryImg = new IplImage(grayCroppedBinaryMat)) {
            myErode(grayCroppedBinaryImg, 3);
            myDilate(grayCroppedBinaryImg, 6);

            try (CvMemStorage storage = CvMemStorage.create(); CvSeq contours = new CvContour()) {
                cvFindContours(grayCroppedBinaryImg, storage, contours, Loader.sizeof(CvContour.class), CV_RETR_CCOMP, CV_CHAIN_APPROX_NONE, new CvPoint(0, 0));

                try (CvSeq eggContour = findBigContour(contours)) {
                    IplImage eggFilledInWhiteImg = new IplImage(eggFilledInWhiteMat);
                    cvDrawContours(eggFilledInWhiteImg, eggContour, CvScalar.WHITE, CV_RGB(0, 0, 0), -1, CV_FILLED, 8, cvPoint(0, 0));
                    return eggFilledInWhiteImg;
                }
            }
        }
    }

    protected CvSeq findBigContour(CvSeq contours) {
        double bigArea = 0;
        CvSeq bigContour = null;

        for (CvSeq ptr = contours; //
             ptr != null && ptr.address() != 0; //
             ptr = ptr.h_next()) {
            double size = cvContourArea(ptr);
            if (size > bigArea) {
                bigArea = size;
                bigContour = ptr;
            }
        }
        return bigContour;
    }

    protected void drawAllContours(IplImage out, CvSeq contours, boolean ignoreSmall, boolean adjustSize, boolean drawContours) {
        final int minimumArea = 850;
        final int plus = 65;

        try (CvPoint p1 = new CvPoint(0, 0); CvPoint p2 = new CvPoint(0, 0)) {

            for (CvSeq ptr = contours; ptr != null; ptr = ptr.h_next()) {
                try (CvRect sq = cvBoundingRect(ptr, 0)) {

                    boolean small = ((sq.width() * sq.height()) > minimumArea);
                    if (!ignoreSmall || small) {
                        int x = adjustSize ? sq.x() - plus : sq.x();
                        int y = adjustSize ? sq.y() - plus : sq.y();

                        p1.x(x);
                        p1.y(y);
                        p2.x(x + (adjustSize ? sq.width() + (plus * 2) : sq.width()));
                        p2.y(y + (adjustSize ? sq.height() + (plus * 2) : sq.height()));

                        if (small) {
                            cvRectangle(out, p1, p2, CV_RGB(0, 0, 255), 2, 8, 0);
                        } else {
                            cvRectangle(out, p1, p2, CV_RGB(255, 0, 0), 2, 8, 0);
                        }
                        if (drawContours) {
                            cvDrawContours(out, ptr, CvScalar.WHITE, CV_RGB(0, 0, 0), -1, CV_FILLED, 8, cvPoint(0, 0));
                        }
                    }
                }
            }
        }
    }

    protected boolean isDirty(IplImage out, CvRect rect) {
        int dirtyCount = 0;
        int size = 0;
        for (int x = 0; x <= rect.width(); x++) {
            for (int y = 0; y <= rect.height(); y++) {
                try (CvScalar cvScalar = getCvScalar(out, x + rect.x(), y + rect.y())) {
                    double red = cvScalar.get(0);
                    double green = cvScalar.get(1);
                    double blue = cvScalar.get(2);
                    if (red > 130 && green > 130 && blue < 150) {
                        dirtyCount++;
                    }
                    ++size;
                }
            }
        }
        // Se 1/3 da imagem for suja
        return dirtyCount > 0 && (size / dirtyCount) > 0.35;
    }

    protected boolean fourConnectivityWhite(IplImage img, CvRect rectangle, int offset) {
        final int white = 255;
        final int allWhite = white * 5;
        final int xLeft = rectangle.x() - offset;
        final int xRight = rectangle.x() + offset;
        final int yTop = rectangle.y() - offset;
        final int yBotton = rectangle.y() + offset;
        final int middleColor = getColor(img, rectangle.x(), rectangle.y());
        final int topColor = yTop > 0 ? getColor(img, rectangle.x(), yTop) : 0;
        final int bottonColor = yBotton <= img.height() ? getColor(img, rectangle.x(), yBotton) : 0;
        final int leftColor = xLeft > 0 ? getColor(img, xLeft, rectangle.y()) : 0;
        final int rightColor = xRight <= img.width() ? getColor(img, xRight, rectangle.y()) : 0;

        return (middleColor + topColor + bottonColor + leftColor + rightColor == allWhite);
    }

    protected boolean eightConnectivityBlack(IplImage img, CvRect rectangle, int offset) {
        final int height = img.height();
        final int width = img.width();
        final int xLeft = rectangle.x() - offset;
        final int xRight = rectangle.x() + offset;
        final int yTop = rectangle.y() - offset;
        final int yBotton = rectangle.y() + offset;
        final int middleColor = getColor(img, rectangle.x(), rectangle.y());
        final int topColor = yTop > 0 ? getColor(img, rectangle.x(), yTop) : 0;
        final int leftTopColor = yTop > 0 && xLeft > 0 ? getColor(img, xLeft, yTop) : 0;
        final int rightTopColor = yTop > 0 && xRight <= width ? getColor(img, xRight, yTop) : 0;
        final int bottonColor = yBotton <= height ? getColor(img, rectangle.x(), yBotton) : 0;
        final int leftBottonColor = yBotton <= height && xLeft > 0 ? getColor(img, xLeft, yTop) : 0;
        final int rightBottonColor = yBotton <= height && xRight <= width ? getColor(img, xRight, yTop) : 0;
        final int leftColor = xLeft > 0 ? getColor(img, xLeft, rectangle.y()) : 0;
        final int rightColor = xRight <= width ? getColor(img, xRight, rectangle.y()) : 0;

        return (middleColor + topColor + leftTopColor + rightTopColor + bottonColor + leftBottonColor + rightBottonColor + leftColor + rightColor == 0);
    }

    protected int getColor(IplImage img, int x, int y) {
        try (CvScalar cvs = getCvScalar(img, x, y)) {
            if (cvs == null) {
                return -1;
            }
            return (int) cvs.get();
        }
    }

    protected CvScalar getCvScalar(IplImage img, int x, int y) {
        try {
            return cvGet2D(img, y, x);
        } catch (Throwable e) {
            return null;
        }
    }

    protected void myErode(@Const CvArr img, int n) {
        for (int i = 0; i < n; i++) {
            cvErode(img, img);
        }
    }

    protected void myDilate(@Const CvArr img, int n) {
        for (int i = 0; i < n; i++) {
            cvDilate(img, img);
        }
    }

    protected void cropAndSaveAllContours(String name, Mat m, CvSeq contours, boolean ignoreSmall, boolean adjustSize) {
        int minimumArea = 850;
        int plus = 65;
        int i = 0;

        for (CvSeq ptr = contours; ptr != null; ptr = ptr.h_next()) {
            try (CvRect sq = cvBoundingRect(ptr, 0)) {
                if (!ignoreSmall || ((sq.width() * sq.height()) > minimumArea)) {
                    int x = adjustSize ? sq.x() - plus : sq.x();
                    int y = adjustSize ? sq.y() - plus : sq.y();
                    int width = adjustSize ? sq.width() + (plus * 2) : sq.width();
                    int height = adjustSize ? sq.height() + (plus * 2) : sq.height();

                    try (Rect rectCrop = new Rect(x, y, width, height)) {
                        try (Mat crop = new Mat(m, rectCrop); IplImage out = new IplImage(crop)) {
                            String n = (++i) + name;
                            String dir = IMG_DIR + n;
                            cvSaveImage(dir, out);
                        }
                    }
                }
            }
        }
    }

}
