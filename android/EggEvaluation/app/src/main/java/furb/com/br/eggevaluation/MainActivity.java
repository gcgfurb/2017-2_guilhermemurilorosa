package furb.com.br.eggevaluation;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    static {
        System.loadLibrary("opencv_core");
        System.loadLibrary("opencv_imgproc");
        System.loadLibrary("opencv_highgui");
        System.loadLibrary("opencv_flann");
        System.loadLibrary("opencv_features2d");
        System.loadLibrary("opencv_calib3d");
        System.loadLibrary("opencv_video");
    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);

        //findViewById(R.id.btnUser).setOnClickListener((view) -> {
        startActivity(new Intent(this, UserActivity.class));
        //});
        /*findViewById(R.id.btnDebug).setOnClickListener((view) -> {
            startActivity(new Intent(this, DebugActivity.class));
        });*/
    }

}
