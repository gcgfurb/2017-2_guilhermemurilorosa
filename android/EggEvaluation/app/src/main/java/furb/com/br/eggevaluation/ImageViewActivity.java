package furb.com.br.eggevaluation;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class ImageViewActivity extends AppCompatActivity {

    private static boolean showDetails;
    private static boolean isCracked;
    private static boolean isDirty;
    private static int poresCount;
    private static Bitmap bitmap;
    private static Runnable job;

    @Override
    public void onCreate(Bundle instance) {
        super.onCreate(instance);
        setContentView(R.layout.image_main);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (job != null) {
            TextView lbCrack = ((TextView) findViewById(R.id.lbCrack));
            lbCrack.setText("Carregando...");
            lbCrack.setGravity(Gravity.CENTER);
            lbCrack.setVisibility(View.VISIBLE);
            findViewById(R.id.lbDirty).setVisibility(View.INVISIBLE);
            findViewById(R.id.lbShell).setVisibility(View.INVISIBLE);

            new Thread(() -> {
                job.run();

                runOnUiThread(() -> {
                    if (showDetails) {
                        findViewById(R.id.lbDirty).setVisibility(View.VISIBLE);
                        findViewById(R.id.lbShell).setVisibility(View.VISIBLE);
                        lbCrack.setGravity(Gravity.LEFT);
                        lbCrack.setText("Quebrado: " + (isCracked ? "sim" : "não"));
                        ((TextView) findViewById(R.id.lbDirty)).setText("Sujo: " + (isDirty ? "sim" : "não"));
                        ((TextView) findViewById(R.id.lbShell)).setText("Qualidade da casca: " + (poresCount > 20 ? (poresCount > 80 ? "ruim" : "média") : "boa"));
                    } else {
                        lbCrack.setVisibility(View.INVISIBLE);
                    }
                    ((ImageView) findViewById(R.id.imageShow)).setImageBitmap(bitmap);
                });

                System.gc();
                job = null;
            }).start();
        }
    }

    public static void setShowDetails(boolean showDetails) {
        ImageViewActivity.showDetails = showDetails;
    }

    public static void setIsCracked(boolean isCracked) {
        ImageViewActivity.isCracked = isCracked;
    }

    public static void setIsDirty(boolean isDirty) {
        ImageViewActivity.isDirty = isDirty;
    }

    public static void setPoresCount(int poresCount) {
        ImageViewActivity.poresCount = poresCount;
    }

    public static void setBitmap(Bitmap b) {
        bitmap = b;
    }

    public static void setJob(Runnable job) {
        ImageViewActivity.job = job;
    }

}
