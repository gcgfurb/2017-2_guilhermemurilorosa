package furb.com.br.eggevaluation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.IdRes;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.bytedeco.javacpp.Loader;
import org.bytedeco.javacpp.opencv_imgproc;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.bytedeco.javacpp.opencv_core.CV_64FC3;
import static org.bytedeco.javacpp.opencv_core.CV_RGB;
import static org.bytedeco.javacpp.opencv_core.CvContour;
import static org.bytedeco.javacpp.opencv_core.CvMemStorage;
import static org.bytedeco.javacpp.opencv_core.CvPoint;
import static org.bytedeco.javacpp.opencv_core.CvRect;
import static org.bytedeco.javacpp.opencv_core.CvScalar;
import static org.bytedeco.javacpp.opencv_core.CvSeq;
import static org.bytedeco.javacpp.opencv_core.IPL_DEPTH_8U;
import static org.bytedeco.javacpp.opencv_core.IplImage;
import static org.bytedeco.javacpp.opencv_core.Mat;
import static org.bytedeco.javacpp.opencv_core.MatExpr;
import static org.bytedeco.javacpp.opencv_core.Rect;
import static org.bytedeco.javacpp.opencv_core.Size;
import static org.bytedeco.javacpp.opencv_core.cvCreateImage;
import static org.bytedeco.javacpp.opencv_core.cvPoint;
import static org.bytedeco.javacpp.opencv_imgcodecs.cvSaveImage;
import static org.bytedeco.javacpp.opencv_imgproc.CV_CHAIN_APPROX_NONE;
import static org.bytedeco.javacpp.opencv_imgproc.CV_FILLED;
import static org.bytedeco.javacpp.opencv_imgproc.CV_RETR_CCOMP;
import static org.bytedeco.javacpp.opencv_imgproc.THRESH_BINARY;
import static org.bytedeco.javacpp.opencv_imgproc.cvBoundingRect;
import static org.bytedeco.javacpp.opencv_imgproc.cvCanny;
import static org.bytedeco.javacpp.opencv_imgproc.cvDrawContours;
import static org.bytedeco.javacpp.opencv_imgproc.cvFindContours;
import static org.bytedeco.javacpp.opencv_imgproc.cvRectangle;
import static org.bytedeco.javacpp.opencv_imgproc.cvtColor;
import static org.bytedeco.javacpp.opencv_imgproc.threshold;

class Core extends MyCV {

    private Activity context;
    private static final int TAKE_PHOTO_CODE = 0;
    private static final int FILE_SELECT_CODE = 1;
    private static final int FILE_SELECT_AND_PROCESS_CODE = 2;
    private int cracksCount = 0;
    private int smallCracksCount = 0;
    private int dirtyCount = 0;
    private int poresCount = 0;
    private String picturePath;

    Core(Activity context) {
        this.context = context;
        new File(IMG_DIR).mkdirs();
    }

    void createListners() {
        onClickCapture();
        onClickChooserAndProcess();
        onClickChooser();
        onClickShow();
        onClickGray();
        onClickBinary();
        onClickCracks();
        onClickRed();
        onClickGreen();
        onClickBlue();
        onClickHue();
        onClickSaturation();
        onClickValue();
        onClickErosion();
        onClickEdge();
        onClickBoundbox();
        onClickROI();
        onClickCrop();
        onClickSub();
        onClickRThresh();
        onClickGThresh();
        onClickBThresh();
        onClickHThresh();
        onClickSThresh();
        onClickVThresh();
        onClickSTPlus();
        onClickEdgePlus();
        onClickSTPRoi();
    }

    private void onClickCapture() {
        setOnClickListenerSafe(R.id.btnCapture, (view) -> {

            picturePath = IMG_DIR + System.currentTimeMillis() + ".jpg";
            try {
                File originalPhoto = new File(picturePath);
                originalPhoto.createNewFile();
                Uri outputFileUri = Uri.fromFile(originalPhoto);
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                cameraIntent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, outputFileUri);
                context.startActivityForResult(cameraIntent, TAKE_PHOTO_CODE);
            } catch (IOException e) {
                Log.d("Camera failure", e.getMessage());
                throw new RuntimeException(e);
            }
        });
    }

    private void onClickChooserAndProcess() {
        setOnClickListenerSafe(R.id.btnChooserAndProcess, (view) -> {

            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("*/*");
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            try {
                context.startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), FILE_SELECT_AND_PROCESS_CODE);
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(context, "Please install a File Manager.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void onClickChooser() {
        setOnClickListenerSafe(R.id.btnChooser, (view) -> {

            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("*/*");
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            try {
                context.startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"), FILE_SELECT_CODE);
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(context, "Please install a File Manager.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void onClickShow() {
        setOnClickListenerSafe(R.id.btnShow, (view) -> runJob(() -> {

            ImageViewActivity.setBitmap(getBitmap(imgPath));
            showImg();
        }));
    }

    private void onClickGray() {
        setOnClickListenerSafe(R.id.btnGray, (view) -> runJob(() -> {

            try (Mat mat = imgRead()) {
                cvtColor(mat, mat, opencv_imgproc.CV_RGB2GRAY);

                try (IplImage img = new IplImage(mat)) {
                    save("egg_gray.jpg", img);
                }
            }
        }));
    }

    private void onClickBinary() {
        setOnClickListenerSafe(R.id.btnBinary, (view) -> runJob(() -> {

            try (Mat mat = imgRead()) {
                cvtColor(mat, mat, opencv_imgproc.CV_RGB2GRAY);
                double thresh = getAvgThreshold(mat);
                threshold(mat, mat, thresh, 255, THRESH_BINARY);

                try (IplImage img = new IplImage(mat)) {
                    save("egg_binary.jpg", img);
                }
            }
        }));
    }

    private void onClickCracks() {
        setOnClickListenerSafe(R.id.btnCracks, (view) -> runJob(() -> {

            try (Mat mat = imgRead(); IplImage src = new IplImage(mat); IplImage img = getGreenChannel(src)) {
                double thresh = getAvgThreshold(img);

                try (Mat mOut = new Mat(img); IplImage iplImageOut = new IplImage(mOut)) {
                    cvCanny(iplImageOut, iplImageOut, thresh, 255, 3);
                    save("egg_cracks.jpg", iplImageOut);
                }
            }
        }));
    }

    private void onClickRed() {
        setOnClickListenerSafe(R.id.btnRed, (view) -> runJob(() -> {

            try (Mat mat = imgRead(); IplImage src = new IplImage(mat); IplImage out = getRedChannel(src)) {
                save("egg_red.jpg", out);
            }
        }));
    }

    private void onClickGreen() {
        setOnClickListenerSafe(R.id.btnGreen, (view) -> runJob(() -> {

            try (Mat mat = imgRead(); IplImage src = new IplImage(mat); IplImage out = getGreenChannel(src)) {
                save("egg_green.jpg", out);
            }
        }));
    }

    private void onClickBlue() {
        setOnClickListenerSafe(R.id.btnBlue, (view) -> runJob(() -> {

            try (Mat mat = imgRead(); IplImage src = new IplImage(mat); IplImage out = getBlueChannel(src)) {
                save("egg_blue.jpg", out);
            }
        }));
    }

    private void onClickHue() {
        setOnClickListenerSafe(R.id.btnHue, (view) -> runJob(() -> {

            try (Mat mat = imgRead(); IplImage src = new IplImage(mat); IplImage out = getHueChannel(src)) {
                save("egg_hue.jpg", out);
            }
        }));
    }

    private void onClickSaturation() {
        setOnClickListenerSafe(R.id.btnSaturation, (view) -> runJob(() -> {

            try (Mat mat = imgRead(); IplImage src = new IplImage(mat); IplImage out = getSaturationChannel(src)) {
                save("egg_saturation.jpg", out);
            }
        }));
    }

    private void onClickValue() {
        setOnClickListenerSafe(R.id.btnValue, (view) -> runJob(() -> {

            try (Mat mat = imgRead(); IplImage src = new IplImage(mat); IplImage out = getValueChannel(src)) {
                save("egg_value.jpg", out);
            }
        }));
    }

    private void onClickErosion() {
        setOnClickListenerSafe(R.id.btnErosion, (view) -> runJob(() -> {

            try (Mat mat = imgRead(); IplImage src = new IplImage(mat); IplImage img = getBlueChannel(src)) {
                double thresh = getAvgThreshold(img);

                try (Mat mOut = new Mat(img)) {
                    threshold(mOut, mOut, thresh, 255, THRESH_BINARY);

                    try (IplImage iplImageOut = new IplImage(mOut)) {
                        save("egg_erosion.jpg", iplImageOut);
                    }
                }
            }
        }));
    }

    private void onClickEdge() {
        setOnClickListenerSafe(R.id.btnEdge, (view) -> runJob(() -> {

            try (Mat mat = imgRead(); IplImage src = new IplImage(mat); IplImage img = getGreenChannel(src)) {
                double thresh = getAvgThreshold(img);

                try (Mat mOut = new Mat(img)) {
                    threshold(mOut, mOut, thresh, 255, THRESH_BINARY);

                    try (IplImage iplImageOut = new IplImage(mOut)) {
                        myErode(iplImageOut, 6);
                        cvCanny(iplImageOut, iplImageOut, 20, 255, 3);
                        myDilate(iplImageOut, 6);
                        save("egg_edge.jpg", iplImageOut);
                    }
                }
            }
        }));
    }

    private void onClickBoundbox() {
        setOnClickListenerSafe(R.id.btnBoundbox, (view) -> runJob(() -> {

            try (Mat mat = imgRead()) {
                cvtColor(mat, mat, opencv_imgproc.CV_RGB2GRAY);
                double thresh = getAvgThreshold(mat);
                threshold(mat, mat, thresh, 255, THRESH_BINARY);

                try (IplImage imgThresh = new IplImage(mat)) {
                    myErode(imgThresh, 3);
                    myDilate(imgThresh, 6);

                    try (CvMemStorage storage = CvMemStorage.create(); CvSeq contours = new CvContour()) {
                        cvFindContours(imgThresh, storage, contours, Loader.sizeof(CvContour.class), CV_RETR_CCOMP, CV_CHAIN_APPROX_NONE, new CvPoint(0, 0));
                        try (CvSeq bigContour = findBigContour(contours); CvPoint p1 = new CvPoint(0, 0); CvPoint p2 = new CvPoint(0, 0)) {
                            try (CvRect sq = cvBoundingRect(bigContour, 0)) {
                                p1.x(sq.x());
                                p2.x(sq.x() + sq.width());
                                p1.y(sq.y());
                                p2.y(sq.y() + sq.height());

                                try (IplImage out = new IplImage(cvCreateImage(mat.cvSize(), IPL_DEPTH_8U, 3))) {
                                    cvRectangle(out, p1, p2, CV_RGB(255, 0, 0), 3, 8, 0);
                                    cvDrawContours(out, bigContour, CvScalar.WHITE, CV_RGB(0, 0, 0), -1, CV_FILLED, 8, cvPoint(0, 0));
                                    save("egg_boundbox.jpg", out);
                                }
                            }
                        }
                    }
                }
            }
        }));
    }

    private void onClickROI() {
        setOnClickListenerSafe(R.id.btnROI, (view) -> runJob(() -> {

            try (Mat mat = imgRead()) {
                cvtColor(mat, mat, opencv_imgproc.CV_RGB2GRAY);
                threshold(mat, mat, getAvgThreshold(mat), 255, THRESH_BINARY);

                try (IplImage imgThresh = new IplImage(mat)) {
                    myErode(imgThresh, 3);
                    myDilate(imgThresh, 6);

                    try (CvMemStorage storage = CvMemStorage.create(); CvSeq contours = new CvContour()) {
                        cvFindContours(imgThresh, storage, contours, Loader.sizeof(CvContour.class), CV_RETR_CCOMP, CV_CHAIN_APPROX_NONE, new CvPoint(0, 0));

                        try (IplImage out = new IplImage(cvCreateImage(mat.cvSize(), IPL_DEPTH_8U, 3))) {
                            drawAllContours(out, contours, false, false, true);
                            save("egg_roi.jpg", out);
                        }
                    }
                }
            }
        }));
    }

    private void onClickCrop() {
        setOnClickListenerSafe(R.id.btnCrop, (view) -> runJob(() -> {

            try (Mat mat = imgRead()) {
                cvtColor(mat, mat, opencv_imgproc.CV_RGB2GRAY);
                double thresh = getAvgThreshold(mat);
                threshold(mat, mat, thresh, 255, THRESH_BINARY);

                try (IplImage imgThresh = new IplImage(mat)) {
                    myErode(imgThresh, 3);
                    myDilate(imgThresh, 6);

                    try (CvMemStorage storage = CvMemStorage.create(); CvSeq contours = new CvContour()) {
                        cvFindContours(imgThresh, storage, contours, Loader.sizeof(CvContour.class), CV_RETR_CCOMP, CV_CHAIN_APPROX_NONE, new CvPoint(0, 0));

                        try (CvSeq bigContour = findBigContour(contours); CvRect sq = cvBoundingRect(bigContour, 0)) {
                            try (Rect rect = new Rect(sq.x(), sq.y(), sq.width(), sq.height())) {
                                try (Mat read = imgRead(); IplImage out = new IplImage(new Mat(read, rect))) {
                                    save("egg_crop.jpg", out);
                                }
                            }
                        }
                    }
                }
            }
        }));
    }

    private void onClickSub() {
        setOnClickListenerSafe(R.id.btnSub, (view) -> runJob(() -> {

            try (Mat mat = imgRead()) {
                cvtColor(mat, mat, opencv_imgproc.CV_RGB2GRAY);
                threshold(mat, mat, 160, 255, THRESH_BINARY);

                try (IplImage imgThresh = new IplImage(mat)) {
                    myErode(imgThresh, 3);
                    myDilate(imgThresh, 6);

                    try (CvMemStorage storage = CvMemStorage.create(); CvSeq contours = new CvContour()) {
                        cvFindContours(imgThresh, storage, contours, Loader.sizeof(CvContour.class), CV_RETR_CCOMP, CV_CHAIN_APPROX_NONE, new CvPoint(0, 0));

                        try (IplImage out = new IplImage(cvCreateImage(mat.cvSize(), IPL_DEPTH_8U, 3))) {
                            drawAllContours(out, contours, false, false, true);
                            save("egg_roi.jpg", out);
                        }
                    }
                }
            }
        }));
    }

    private void onClickRThresh() {
        setOnClickListenerSafe(R.id.btnRThresh, (view) -> runJob(() -> {

            try (Mat mat = imgRead(); IplImage src = new IplImage(mat); IplImage img = getRedChannel(src)) {
                double thresh = getAvgThreshold(img);

                try (Mat mOut = new Mat(img)) {
                    threshold(mOut, mOut, thresh, 255, THRESH_BINARY);

                    try (IplImage out = new IplImage(mOut)) {
                        save("egg_rthresh.jpg", out);
                    }
                }
            }
        }));
    }

    private void onClickGThresh() {
        setOnClickListenerSafe(R.id.btnGThresh, (view) -> runJob(() -> {

            try (Mat mat = imgRead(); IplImage src = new IplImage(mat); IplImage img = getGreenChannel(src)) {
                double thresh = getAvgThreshold(img);

                try (Mat mOut = new Mat(img)) {
                    threshold(mOut, mOut, thresh, 255, THRESH_BINARY);

                    try (IplImage out = new IplImage(mOut)) {
                        save("egg_gthresh.jpg", out);
                    }
                }
            }
        }));
    }

    private void onClickBThresh() {
        setOnClickListenerSafe(R.id.btnBThresh, (view) -> runJob(() -> {

            try (Mat mat = imgRead(); IplImage src = new IplImage(mat); IplImage img = getBlueChannel(src)) {
                double thresh = getAvgThreshold(img);

                try (Mat mOut = new Mat(img)) {
                    threshold(mOut, mOut, thresh, 255, THRESH_BINARY);

                    try (IplImage out = new IplImage(mOut)) {
                        save("egg_bthresh.jpg", out);
                    }
                }
            }
        }));
    }

    private void onClickHThresh() {
        setOnClickListenerSafe(R.id.btnHThresh, (view) -> runJob(() -> {

            try (Mat mat = imgRead(); IplImage src = new IplImage(mat); IplImage img = getHueChannel(src)) {
                try (Mat mOut = new Mat(img)) {
                    threshold(mOut, mOut, 70, 255, THRESH_BINARY);

                    try (IplImage out = new IplImage(mOut)) {
                        save("egg_hthresh.jpg", out);
                    }
                }
            }
        }));
    }

    private void onClickSThresh() {
        setOnClickListenerSafe(R.id.btnSThresh, (view) -> runJob(() -> {

            try (Mat mat = imgRead(); IplImage src = new IplImage(mat); IplImage img = getSaturationChannel(src); Mat mOut = new Mat(img)) {
                threshold(mOut, mOut, 70, 255, THRESH_BINARY);

                try (IplImage out = new IplImage(mOut)) {
                    save("egg_sthresh.jpg", out);
                }
            }
        }));
    }

    private void onClickVThresh() {
        setOnClickListenerSafe(R.id.btnVThresh, (view) -> runJob(() -> {

            try (Mat mat = imgRead(); IplImage src = new IplImage(mat); IplImage img = getValueChannel(src); Mat mOut = new Mat(img)) {
                threshold(mOut, mOut, 70, 255, THRESH_BINARY);

                try (IplImage out = new IplImage(mOut)) {
                    save("egg_vthresh.jpg", out);
                }
            }
        }));
    }

    private void onClickSTPlus() {
        setOnClickListenerSafe(R.id.btnSThreshPlus, (view) -> imageProcess());
    }

    private void imageProcess() {
        runJob(() -> {
            cracksCount = 0;
            smallCracksCount = 0;
            dirtyCount = 0;
            poresCount = 0;
            try (Mat srcMat = imgRead(); Mat srcGrayMat = convertToGray(srcMat); Mat tempGrayMat = srcGrayMat.clone(); IplImage grayImg = new IplImage(tempGrayMat)) {
                double grayThreshold = getAvgThreshold(grayImg);

                try (Mat binaryMat = myThreshold(grayImg, grayThreshold); Rect eggRectangle = getEggRoiRectangle(binaryMat)) {
                    try (Mat scrCroppedMat = new Mat(srcMat, eggRectangle); IplImage srcCroppedImg = new IplImage(scrCroppedMat)) {
                        try (IplImage saturationImg = getSaturationChannel(srcCroppedImg)) {
                            try (Mat saturationBinaryMat = myThreshold(saturationImg, 55d); IplImage saturationBinaryImg = new IplImage(saturationBinaryMat)) {
                                myErode(saturationBinaryImg, 1);
                                myDilate(saturationBinaryImg, 3);

                                try (IplImage outImg = new IplImage(scrCroppedMat); Mat grayCroppedMat = new Mat(srcGrayMat, eggRectangle); Mat grayCroppedTempMat = grayCroppedMat.clone()) {
                                    try (MatExpr eggContourExpr = Mat.zeros(new Size(eggRectangle.width(), eggRectangle.height()), CV_64FC3)) {
                                        try (Mat eggFilledInWhiteMat = eggContourExpr.asMat()) {
                                            try (IplImage eggFilledInWhiteImg = getEggFilledInWhiteImg(grayCroppedTempMat, eggFilledInWhiteMat, grayThreshold)) {
                                                findCrackes(outImg, grayCroppedMat, grayThreshold);
                                                findImperfections(eggFilledInWhiteImg, outImg, saturationBinaryImg);
                                                save("egg_stplus.jpg", outImg);
                                                ImageViewActivity.setShowDetails(true);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });
    }

    private void onClickEdgePlus() {
        setOnClickListenerSafe(R.id.btnEdgePlus, (view) -> runJob(() -> {

            try (Mat mat = imgRead(); IplImage src = new IplImage(mat); Mat mGray = new Mat(src)) {
                cvtColor(mGray, mGray, opencv_imgproc.CV_RGB2GRAY);
                double thresh = getAvgThreshold(mGray);

                try (IplImage imageGray = new IplImage(mGray)) {
                    cvCanny(imageGray, imageGray, thresh, 255, 3);
                    myDilate(imageGray, 10);
                    try (CvMemStorage storageCrackes = CvMemStorage.create(); CvSeq contoursCrackes = new CvContour()) {
                        cvFindContours(imageGray, storageCrackes, contoursCrackes, Loader.sizeof(CvContour.class), CV_RETR_CCOMP, CV_CHAIN_APPROX_NONE, new CvPoint(0, 0));
                        cracksCount = 0;
                        for (CvSeq ptr = contoursCrackes; ptr != null; ptr = ptr.h_next()) {
                            cracksCount++;
                            cvDrawContours(src, ptr, CvScalar.BLUE, CV_RGB(0, 0, 0), -1, CV_FILLED, 8, cvPoint(0, 0));
                        }
                        save("egg_edge_plus.jpg", src);
                    }
                }
            }
        }));
    }


    private void onClickSTPRoi() {
        setOnClickListenerSafe(R.id.btnSTPRoi, (view) -> runJob(() -> {

            try (Mat mat = imgRead(); IplImage src = new IplImage(mat); Mat mCrop = new Mat(src)) {
                cvtColor(mCrop, mCrop, opencv_imgproc.CV_RGB2GRAY);

                try (IplImage imgCrop = new IplImage(mCrop)) {
                    double thresh = getAvgThreshold(imgCrop);
                    threshold(mCrop, mCrop, thresh, 255, THRESH_BINARY);

                    try (IplImage imgTemp = new IplImage(mCrop)) {
                        myErode(imgTemp, 3);
                        myDilate(imgTemp, 6);

                        try (CvMemStorage storageCrop = CvMemStorage.create(); CvSeq contoursCrop = new CvContour()) {
                            cvFindContours(imgTemp, storageCrop, contoursCrop, Loader.sizeof(CvContour.class), CV_RETR_CCOMP, CV_CHAIN_APPROX_NONE, new CvPoint(0, 0));
                            try (CvSeq bigContour = findBigContour(contoursCrop); CvRect sqCrop = cvBoundingRect(bigContour, 0)) {
                                try (Rect rectCrop = new Rect(sqCrop.x(), sqCrop.y(), sqCrop.width(), sqCrop.height())) {
                                    try (IplImage hsv = getSaturationChannel(src); IplImage img = new IplImage(hsv); Mat m = new Mat(img)) {
                                        threshold(m, m, 65, 255, THRESH_BINARY);

                                        try (Mat scrCrop = new Mat(mat, rectCrop); Mat crop = new Mat(m, rectCrop); IplImage cropped = new IplImage(crop)) {
                                            myErode(cropped, 1);
                                            myDilate(cropped, 3);
                                            try (CvMemStorage storage = CvMemStorage.create(); CvSeq contours = new CvContour()) {
                                                cvFindContours(cropped, storage, contours, Loader.sizeof(CvContour.class), CV_RETR_CCOMP, CV_CHAIN_APPROX_NONE, new CvPoint(0, 0));

                                                cropAndSaveAllContours("egg_stproi.jpg", scrCrop, contours, true, true);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }));
    }

    private void setOnClickListenerSafe(@IdRes int id, View.OnClickListener l) {
        View v = context.findViewById(id);
        if (v != null) {
            v.setOnClickListener(l);
        }
    }

    private void findCrackes(IplImage outputImg, Mat grayMat, double grayThreshold) {
        // Este número mágico é baseado numa regra de três do número 850 a partir de uma imagem 2806 de altura e 2098 de largura
        final int minimumArea = (int) ((outputImg.width() * outputImg.height()) / 6_925.885);

        try (IplImage imageGray = new IplImage(grayMat)) {
            cvCanny(imageGray, imageGray, grayThreshold, 255, 3);
            myDilate(imageGray, 5);
            try (CvMemStorage storageCrackes = CvMemStorage.create(); CvSeq contoursCrackes = new CvContour()) {
                cvFindContours(imageGray, storageCrackes, contoursCrackes, Loader.sizeof(CvContour.class), CV_RETR_CCOMP, CV_CHAIN_APPROX_NONE, new CvPoint(0, 0));
                for (CvSeq ptr = contoursCrackes; ptr != null; ptr = ptr.h_next()) {
                    try (CvRect rectangle = cvBoundingRect(ptr, 0)) {
                        boolean containsMinimumArea = ((rectangle.width() * rectangle.height()) > minimumArea);

                        if (containsMinimumArea) {
                            cracksCount++;
                            cvDrawContours(outputImg, ptr, CvScalar.BLUE, CV_RGB(0, 0, 0), -1, CV_FILLED, 8, cvPoint(0, 0));
                        }
                    }
                }
            }
        }
    }

    private void findImperfections(IplImage eggFilledInWhiteImg, IplImage outputImg, IplImage saturationBinaryImg) {
        try (CvMemStorage imperfectionsStorage = CvMemStorage.create(); CvSeq imperfectionsContours = new CvContour()) {
            cvFindContours(saturationBinaryImg, imperfectionsStorage, imperfectionsContours, Loader.sizeof(CvContour.class), CV_RETR_CCOMP, CV_CHAIN_APPROX_NONE, new CvPoint(0, 0));
            int height = outputImg.height();
            int width = outputImg.width();

            // Este número mágico é baseado numa regra de três do número 65 a partir de uma imagem 2806 de altura e 2098 de largura
            final int plus = (int) ((width * height) / 90_569.262);
            // Este número mágico é baseado numa regra de três do número 55 a partir de uma imagem 2806 de altura e 2098 de largura
            final int eggEdgeOffset = (int) ((width * height) / 107_036.4);

            if (!imperfectionsContours.isNull()) {
                try (CvPoint p1 = new CvPoint(0, 0); CvPoint p2 = new CvPoint(0, 0)) {

                    for (CvSeq ptr = imperfectionsContours; ptr != null; ptr = ptr.h_next()) {
                        try (CvRect rectangle = cvBoundingRect(ptr, 0)) {

                            p1.x(rectangle.x());
                            p1.y(rectangle.y());
                            p2.x(rectangle.x() + rectangle.width());
                            p2.y(rectangle.y() + rectangle.height());

                            if (fourConnectivityWhite(eggFilledInWhiteImg, rectangle, eggEdgeOffset)) {
                                if (isDirty(outputImg, rectangle)) {
                                    drawDirty(outputImg, ptr);
                                }
                            } else {
                                if (!eightConnectivityBlack(eggFilledInWhiteImg, rectangle, 1)) {
                                    drawPores(outputImg, ptr);
                                }
                            }
                        } catch (Throwable e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    private void drawPores(IplImage outputImg, CvSeq ptr) {
        ++poresCount;
        cvDrawContours(outputImg, ptr, CvScalar.GREEN, CV_RGB(0, 0, 0), -1, CV_FILLED, 8, cvPoint(0, 0));
    }

    private void drawDirty(IplImage outputImg, CvSeq ptr) {
        ++dirtyCount;
        cvDrawContours(outputImg, ptr, CvScalar.YELLOW, CV_RGB(0, 0, 0), -1, CV_FILLED, 8, cvPoint(0, 0));
    }

    private void save(String name, IplImage img) {
        cvSaveImage(IMG_DIR + name, img);
        ImageViewActivity.setIsCracked((smallCracksCount + cracksCount) > 0);
        ImageViewActivity.setIsDirty(dirtyCount > 0);
        ImageViewActivity.setPoresCount(poresCount);
        ImageViewActivity.setBitmap(getBitmap(IMG_DIR + name));
    }

    protected static Bitmap getBitmap(String imgPath) {
        return BitmapFactory.decodeFile(getFile(imgPath).getAbsolutePath());
    }

    private static File getFile(String imgPath) {
        File file = new File(imgPath);
        if (!file.exists()) {
            throw new IllegalArgumentException(new FileNotFoundException());
        }
        return file;
    }

    private void runJob(Runnable runnable) {
        ImageViewActivity.setShowDetails(false);
        loadParameters();
        ImageViewActivity.setJob(runnable);
        showImg();
    }

    private void showImg() {
        context.startActivity(new Intent(context, ImageViewActivity.class));
    }

    private void loadParameters() {
        threshold = 0;

        View vImgName = context.findViewById(R.id.edtImgName);
        if (vImgName != null) {
            imgPath = ((EditText) vImgName).getText().toString();
        }
        View vThreshold = context.findViewById(R.id.edtThreshold);
        if (vThreshold != null) {
            threshold = Integer.valueOf(((EditText) vThreshold).getText().toString());
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, android.content.Intent data) {
        if (requestCode == TAKE_PHOTO_CODE && resultCode == Activity.RESULT_OK) {
            imgPath = picturePath;
            imageProcess();
            Log.d(TAG, "Pic saved");
        } else if ((requestCode == FILE_SELECT_CODE || requestCode == FILE_SELECT_AND_PROCESS_CODE) && resultCode == Activity.RESULT_OK) {
            Uri uri = data.getData();
            Log.d(TAG, "File Uri: " + uri.toString());
            File file = new File(getRealPathFromDocumentUri(context, uri));
            imgPath = file.getAbsolutePath();

            if (requestCode == FILE_SELECT_CODE) {
                ((EditText) context.findViewById(R.id.edtImgName)).setText(imgPath);
            } else if (requestCode == FILE_SELECT_AND_PROCESS_CODE) {
                imageProcess();
            }
        }
    }

    private static String getRealPathFromDocumentUri(Context context, Uri uri) {
        String filePath = "";
        Pattern p = Pattern.compile("(\\d+)$");
        Matcher m = p.matcher(uri.toString());

        if (!m.find()) {
            Log.e(TAG, "ID for requested image not found: " + uri.toString());
            return filePath;
        }
        String imgId = m.group();
        String[] column = {MediaStore.Images.Media.DATA};
        String sel = MediaStore.Images.Media._ID + "=?";
        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, column, sel, new String[]{imgId}, null);
        if (cursor != null) {
            int columnIndex = cursor.getColumnIndex(column[0]);

            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
        }
        return filePath;
    }

}
